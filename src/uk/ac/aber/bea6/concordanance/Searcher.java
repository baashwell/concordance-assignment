package uk.ac.aber.bea6.concordanance;
import java.util.ArrayList;

/**
 * @author Ben Ashwell
 *
 */
public class Searcher 
{
	/** Constant for the amount of words for context*/
	private static final int CONTEXT_CONSTANT = 3;
	/**
	 * General constructor for searcher
	 */
	public Searcher()
	{
	}
	
	/**
	 * Method to search for a word in the line that have both been given and return an element.
	 * @param word Word to search for.
	 * @param line Line to search through.
	 * @param lineNumber Line number of document currently processing, used to make element.
	 * @return Element containing the line number and context.
	 */
	public ArrayList<Element> searchForWord(String word, String line, int lineNumber)
	{
		ArrayList<Element> retual = new ArrayList<Element>();
		String context = "";
		
		//split into words.
		String[] temp = line.split(" ");
		
		//for every word in the line check if the words are equal.
		for(int i = 0; i < temp.length; i++)
		{
			if(word.equals(temp[i]))
			{
				//for the word constant either side of found word check it is within temp bounds
				//and then add the word with a space to context
				for(int j = i - CONTEXT_CONSTANT; j < i + CONTEXT_CONSTANT; j++)
				{
					if(j >= 0 && j < temp.length)
					{
						context = context + temp[j] + " ";
					}
				}
				retual.add(new Element(lineNumber, context));
			}
		}		
		return retual;
	}
}
