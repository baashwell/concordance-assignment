package uk.ac.aber.bea6.concordanance;
import java.io.*;
import java.util.ArrayList;

/**
 * @author Ben Ashwell
 *
 */
public class Reader
{
	/** Constant for the default file name */
	private static final String DEFAULT_FILE = "index.txt";
	/** File reader object to read in the files specified later */
	private FileReader fileR;
	/** Buffered Reader to run on top of File Reader. */
	private BufferedReader bufferR;
	
	/**
	 * Basic constructor that passes the DEFAULT_FILE to other constructor
	 */
	public Reader()
	{
		this(DEFAULT_FILE);
	}
	
	/**
	 * Constructor to set buffer reader up with the file reader and designated file.
	 * @param fileName
	 */
	public Reader(String fileName)
	{
		try 
		{
			bufferR	= new BufferedReader(fileR = new FileReader(fileName));
		} 
		catch (FileNotFoundException e) 
		{
			//Do Nothing as this will cause empty variable to stay true
			//which will show the document error message
		}
					
	}
	
	/**
	 * Method to read lines of a file and store each line separately in an ArrayList
	 * @return ArrayList containing Strings of each line of file.
	 */
	public ArrayList<String> readLines()
	{
		ArrayList<String> retual = new ArrayList<String>();
		String line;
		
		try 
		{
			while((line = bufferR.readLine()) != null)
			{
				retual.add(line);				
			}
		} 
		catch (IOException e) 
		{
			//Do Nothing as this will cause empty variable to stay true
			//which will show the document error message 			
		}
		
		return retual;
	}
	
	/**
	 * Method to read a single line of a file and return it as a string
	 * @return Next line of a file as a String
	 */
	public String readLine()
	{
		String retual = null;
		try 
		{
			retual = bufferR.readLine();
		} 
		catch (IOException e) 
		{
			//Do Nothing as this will cause empty variable to stay true
			//which will show the document error message
		}
		
		return retual;
	}
	
	/**
	 * Method to close all readers after use
	 */
	public void closeReaders()
	{
		try 
		{
			fileR.close();
			bufferR.close();
		} 
		catch (IOException e) 
		{
		}
		
	}

}
