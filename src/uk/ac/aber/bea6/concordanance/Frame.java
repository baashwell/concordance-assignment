package uk.ac.aber.bea6.concordanance;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

/**
 * @author Ben Ashwell
 *
 */
public class Frame extends JFrame implements WindowListener
{
	/** Menu bar for frame */
	private JMenuBar menuBar;
	/** Menu file for menu bar */
	private JMenu file;
	/** Menu item for the menu file */
	private JMenuItem exit;
	/** Menu Listener for the menu on frame */
	private MenuListener menuList;
	/** Button to find words */
	private JButton find;
	/** Text area to display words */
	private JTextArea textBlock;
	/** Scroll Pane for the text block */
	private JScrollPane textBlockScroll;
	/** Button Listener for the find button */
	private ButtonListener buttonListener;
	/** Container object to link back */
	private Container container;
	
	/**
	 * Constructor for the frame object, takes a container to link back to code
	 * @param container Container object to link back and do code
	 */
	public Frame(Container container)
	{
		//set container and title, also make window listener to apply so closing operations
		this.container = container;
		this.setTitle("Concordance Search");
		this.setPreferredSize(new Dimension(500 , 500));
		addWindowListener(this);
		
		//add menu, button and text area adding listeners and setting GUi up generally
		menuBar = new JMenuBar();
		menuList = new MenuListener(this);
		file = new JMenu("File");
		exit = new JMenuItem("Exit");
		find = new JButton("Find");
		buttonListener = new ButtonListener(this);
		textBlock = new JTextArea();
		textBlockScroll = new JScrollPane(textBlock);
		
		this.setJMenuBar(menuBar);
		menuBar.add(file);
		file.add(exit);
		exit.addActionListener(menuList);
		
		add(find, BorderLayout.NORTH);
		find.addActionListener(buttonListener);		
		textBlock.setEditable(false);
		add(textBlockScroll, BorderLayout.CENTER);
		
		pack();
		setVisible(true);
	}

	/**
	 * Method to run the search methods in the container, called from button listener
	 */
	public void runSearch()
	{
		ArrayList<String> searchWords;
		String elements = "";
		
		//run the search in container to populate the hash table
		container.runSearch();
		
		if(container.isEmpty() == false)
		{
			searchWords = container.sortAndSendWords();
		
			//for loop to set up the string to print out onto text block
			for(int i = 0; i < searchWords.size(); i++)
			{
				elements = elements + "\n" + searchWords.get(i) + "\n\n" + container.getElements(searchWords.get(i));
			}	
		}
		else
		{
			elements = "There has been a problem with the documents you have entered. Please check the documents and try again.";
		}
		
		textBlock.insert(elements, 0);
		container.closeReaders();
	}
	
	/**
	 * Method to clear the text in the text block
	 */
	public void clearText()
	{
		textBlock.setText("");
		container.clearElements();
	}
	
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}
		
	@Override
	public void windowClosing(WindowEvent e) 
	{
		int answer = JOptionPane.showConfirmDialog(this, "Are you sure?", "Exit Confirmation", JOptionPane.YES_NO_OPTION);
		if (answer == JOptionPane.YES_OPTION)
		{
		   	System.exit(0);
		} 		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
