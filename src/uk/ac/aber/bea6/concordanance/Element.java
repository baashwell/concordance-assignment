package uk.ac.aber.bea6.concordanance;
/**
 * @author Ben Ashwell
 *
 */
public class Element
{
	/** line number this element is found at */
	private int lineNumber;
	/** context of this element */
	private String context;
	
	/**
	 * set up element with the line number and the context
	 * @param lineNumber
	 * @param context
	 */
	public Element(int lineNumber, String context)
	{
		this.lineNumber = lineNumber;
		this.context = context;		
	}
	
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(int lineNumber)
	{
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the lineNumber
	 */
	public int getLineNumber()
	{
		return lineNumber;
	}
	/**
	 * @param context the context to set
	 */
	public void setContext(String context)
	{
		this.context = context;
	}
	/**
	 * @return the context
	 */
	public String getContext() 
	{
		return context;
	}
	
	/**
	 * method to put element as a string
	 */
	public String toString()
	{		
		return lineNumber + " - " + context + "\n";
		
	}

}
