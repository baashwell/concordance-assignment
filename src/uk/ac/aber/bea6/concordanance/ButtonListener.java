package uk.ac.aber.bea6.concordanance;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Ben Ashwell
 *
 */
public class ButtonListener implements ActionListener
{
	/**
	 * Object to link back to the Frame
	 */
	private Frame frame;
	
	/**
	 * Constructor to setup link back to the frame.
	 * @param frame Frame creating button listener
	 */
	public ButtonListener(Frame frame)
	{
		this.frame = frame;
	}
	
	/**
	 * Method to pick up when button is pressed and act accordingly
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		frame.clearText();
		frame.runSearch();
	}
	
	
}
