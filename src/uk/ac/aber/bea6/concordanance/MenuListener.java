package uk.ac.aber.bea6.concordanance;
import java.awt.event.*;
import javax.swing.JOptionPane;

/**
 * @author Ben Ashwell
 *
 */
public class MenuListener implements ActionListener
{
	/** Object to link back to the frame */
	private Frame frame;
	
	/**
	 * The Menu Listener Constructor Links the Listener to the Frame
	 * @param frame Frame object of project
	 */
	MenuListener(Frame frame)
	{
		this.frame = frame;
	}
	
	/**
	 * The actionPerformed Method is implemented to pick up when the menu is used and to
	 * act accordingly.
	 */
	public void actionPerformed(ActionEvent e)
	{
		String actionCommand = e.getActionCommand();
		
		if(actionCommand.equals("Exit"))
		{
			int answer = JOptionPane.showConfirmDialog(frame, "Are you sure?", "Exit Confirmation", JOptionPane.YES_NO_OPTION);
			if (answer == JOptionPane.YES_OPTION)
			{
			   	System.exit(0);
			} 
		}
	}
}