package uk.ac.aber.bea6.concordanance;
/**
 * @author Ben Ashwell
 *
 */
public class Main 
{

	public static void main(String[] args) 
	{
		Container c = new Container();
		Frame f = new Frame(c);
		c.setHashTable();
	}
}
