package uk.ac.aber.bea6.concordanance;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author Ben
 *
 */
public class Container 
{
	/** HashTable to hold the String key and the ArrayList of elements */
	private Hashtable <String, ArrayList<Element>> hashTable;
	/** Searcher object to run search methods on data */
	private Searcher search;
	/** Reader object to read in lines of files */
	private Reader read;
	/** empty boolean to test when find is pressed for if any files where empty */
	private boolean empty = true;
	
	/**
	 * Constructor which sets up the objects of this class
	 */
	public Container()
	{
		hashTable = new Hashtable<String, ArrayList<Element>>();
		search = new Searcher();
		read = new Reader();
	}
	
	/**
	 * @return the empty
	 */
	public boolean isEmpty() 
	{
		return empty;
	}
	
	/**
	 * Method to set up the hash table with files from the word index file
	 */
	public void setHashTable()
	{
		ArrayList<String> temp = read.readLines();
	
		for(int i = 0; i < temp.size(); i++)
		{
			hashTable.put(temp.get(i), new ArrayList<Element>());
			empty = false;
		}
	}
	
	/**
	 * Method to search through the book and update hash tables with appropriate elements
	 */
	public void runSearch()
	{
		//change reader file
		read = new Reader("Book.txt");	
		String line;
		int lineNumber = 0;
		String hashKey;
		ArrayList<Element> returned;
		
			
		while((line = read.readLine()) != null)
		{
			//increase line number first so doesn't start at 0 and get keys from the hash table	
			lineNumber++;
			Enumeration<String> keysCollection = hashTable.keys();
			
			//for each element in the hash table search through the line for each word using search object
			for(int i = 0; i < hashTable.size(); i++)
			{
				//get next key and run search for word in line
				hashKey = keysCollection.nextElement();
				returned = search.searchForWord(hashKey, line, lineNumber);
				
				//for every element returned get the Array list for the hash key and add the elements
				for(int j = 0; j < returned.size(); j++)
				{
					ArrayList<Element> elementList = hashTable.get(hashKey);
					elementList.add(returned.get(j));
				}			
			}
		}
		
		//if there is nothing in the book then return false
		if(lineNumber == 0)
		{
			empty = true;
		}
	}
	
	/**
	 * Method to sort the hash words into alphabetical order and send them in an array list of strings
	 * @return return array list of strings with all keys in alphabetical order.
	 */
	public ArrayList<String> sortAndSendWords()
	{
		ArrayList<String> retual = new ArrayList<String>();
		Enumeration<String> keysCollection = hashTable.keys();
		
		//For every element in the hashTable get an element and add it accordingly
		for(int i = 0; i < hashTable.size(); i++)
		{
			String s = keysCollection.nextElement();
			//if there is not anything in the list then just add it			
			if(retual.isEmpty())
			{
				retual.add(s);
			}
			//otherwise run through each element that is in the list so far and see if it is in
			//a higher alphabetical order, if it is then add it otherwise add at the end
			else
			{
				for(int j = 0; j < retual.size(); j++)
				{
					int comparisonValue = s.compareTo(retual.get(j));
					//if comes before current value add
					if(comparisonValue <= 0)
					{
						retual.add(j, s);
						break;
					}
					//if you get to the end then add at the end
					if(j == retual.size() - 1)
					{
						retual.add(s);
						break;
						
					}
				}
			}
		}				
		return retual;		
	}

	/**
	 * Method to get the elements of a given word
	 * @param string Key word to get elements of
	 * @return return string of all elements together
	 */
	public String getElements(String string)
	{
		String retual = "";
		ArrayList<Element> elements;
		
		//get elements arrayList at key word location
		elements = hashTable.get(string);
		
		//for each element turn it to a string and add it to return
		for(int i = 0; i < elements.size(); i++)
		{
			retual = retual + elements.get(i).toString();
		}
		
		return retual;
	}
	
	/**
	 * Method to clear all elements in the hashTable
	 */
	public void clearElements()
	{
		Enumeration<String> keysCollection = hashTable.keys();
		
		//For every element in the hashTable get an element and clear it
		for(int i = 0; i < hashTable.size(); i++)
		{
			String s = keysCollection.nextElement();
			
			ArrayList temp = hashTable.get(s);
			temp.clear();
		}				
	}
	
	/**
	 * Method to close the readers
	 */
	public void closeReaders()
	{
		read.closeReaders();
	}
}
